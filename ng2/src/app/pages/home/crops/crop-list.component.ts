import { Component, OnInit, EventEmitter } from '@angular/core';
import { Crop } from '../../../models/crop';
import { CropService } from '../../../services/crop.service';

@Component({
  selector: 'app-crop-list',
  templateUrl: './crop-list.component.html',
  styleUrls: ['./crop-list.component.css']
})
export class CropListComponent implements OnInit {
  crops: Crop[] = [];
  selectedCrop: Crop;
  inEditMode: boolean;

  constructor(private service: CropService) { }

  ngOnInit(): void {
    this.reset();
  }

  public select(crop: Crop): void {
    this.getCropById(crop.id);
  }

  private getCrops(): void {
    this.service.all().subscribe((response) => {
      this.crops = response;
    });
  }

  public cropChange(event): void {
    this.reset();
  }

  private getCropById(id: number) {
    this.service.get(id).subscribe((response) => {
      this.selectedCrop = response;
      this.inEditMode = true;
    });
  }

  private reset() {
    this.selectedCrop = this.service.reset();
    this.getCrops();
    this.inEditMode = false;
  }
}
