import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Crop } from '../../../../models/crop';
import { CropService } from '../../../../services/crop.service';

@Component({
  selector: 'app-add-crops',
  templateUrl: './add-crops.component.html',
  styleUrls: ['./add-crops.component.css']
})
export class AddCropsComponent implements OnInit {
  @Input() crop: Crop;
  @Input() inEditMode: boolean;
  @Output() cropChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private service: CropService) { }

  ngOnInit() {
    this.reset();
  }

  public submit(crop: Crop): void {
    this.service.post(crop).subscribe((response) => {
      this.reset();
      this.cropChange.emit(true);
    })
  }

  public reset(): void {
    this.crop = this.service.reset();
    this.inEditMode = false;
  }
}
