import { Component, OnInit } from '@angular/core';
import { Manager } from 'app/models/manager';
import { ManagerService } from 'app/services/manager.service';

@Component({
  selector: 'app-manager-list',
  templateUrl: './manager-list.component.html',
  styleUrls: ['./manager-list.component.css']
})
export class ManagerListComponent implements OnInit {

  items: Manager[] = [];
  selected: Manager;

  constructor(private service: ManagerService) { }

  ngOnInit(): void {
    this.reset();
  }

  public select(manager: Manager): void {
    this.getById(manager.id);
  }

  private list(): void {
    this.service.all().subscribe((response) => {
      this.items = response;
    });
  }

  public submitted(event): void {
    this.reset();
  }

  private getById(id: number) {
    this.service.get(id).subscribe((response) => {
      this.selected = response;
    });
  }

  private reset() {
    this.selected = this.service.reset();
    this.list();
  }
}