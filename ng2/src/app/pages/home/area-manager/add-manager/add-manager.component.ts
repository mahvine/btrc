import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Entity } from 'app/models/entity';
import { EntityService } from 'app/services/entity.service';
import { Manager } from 'app/models/manager';
import { ManagerService } from 'app/services/manager.service';


@Component({
  selector: 'app-add-manager',
  templateUrl: './add-manager.component.html',
  styleUrls: ['./add-manager.component.css']
})
export class AddManagerComponent implements OnInit {

  @Input() manager: Manager;
  @Output() managerSubmitted: EventEmitter<boolean> = new EventEmitter<boolean>();
  entities: Entity[];
  constructor(private service: ManagerService, private entityService: EntityService) { }


  ngOnInit() {
    this.reset();
    this.entityService.all().subscribe(response => {
      this.entities = response;
    });
  }

  public submit(entity: Manager): void {
    this.service.post(entity).subscribe((response) => {
      this.reset();
      this.managerSubmitted.emit(true);
    })
  }

  public reset(): void {
    this.manager = this.service.reset();
  }

}
