import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Entity } from '../../../models/entity';
import { EntityService } from 'app/services/entity.service';

@Component({
  selector: 'app-entity-list',
  templateUrl: './entity-list.component.html',
  styleUrls: ['./entity-list.component.css']
})
export class EntityListComponent implements OnInit {

  items: Entity[] = [];
  selected: Entity;

  constructor(private service: EntityService) { }

  ngOnInit(): void {
    this.reset();
  }

  public select(entity: Entity): void {
    this.getById(entity.id);
  }

  private list(): void {
    this.service.all().subscribe((response) => {
      this.items = response;
    });
  }

  public submitted(event): void {
    this.reset();
  }

  private getById(id: number) {
    this.service.get(id).subscribe((response) => {
      this.selected = response;
    });
  }

  private reset() {
    this.selected = this.service.reset();
    this.list();
  }
}