import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Entity } from 'app/models/entity';
import { EntityService } from 'app/services/entity.service';

@Component({
  selector: 'app-add-entity',
  templateUrl: './add-entity.component.html',
  styleUrls: ['./add-entity.component.css']
})
export class AddEntityComponent implements OnInit {

  @Input() entity: Entity;
  @Output() entitySubmitted: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private service: EntityService) { }

  ngOnInit() {
    this.reset();
  }

  public submit(entity: Entity): void {
    this.service.post(entity).subscribe((response) => {
      this.reset();
      this.entitySubmitted.emit(true);
    })
  }

  public reset(): void {
    this.entity = this.service.reset();
  }
}
