import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Entity } from 'app/models/entity';
import { EntityService } from 'app/services/entity.service';
import { Supervisor } from 'app/models/supervisor';
import { SupervisorService } from 'app/services/supervisor.service';

@Component({
  selector: 'app-add-supervisor',
  templateUrl: './add-supervisor.component.html',
  styleUrls: ['./add-supervisor.component.css']
})
export class AddSupervisorComponent implements OnInit {

  @Input() supervisor: Supervisor;
  @Output() supervisorSubmitted: EventEmitter<boolean> = new EventEmitter<boolean>();
  entities: Entity[];
  constructor(private service: SupervisorService, private entityService: EntityService) { }


  ngOnInit() {
    this.reset();
    this.entityService.all().subscribe(response => {
      this.entities = response;
    });
  }

  public submit(entity: Supervisor): void {
    this.service.post(entity).subscribe((response) => {
      this.reset();
      this.supervisorSubmitted.emit(true);
    })
  }

  public reset(): void {
    this.supervisor = this.service.reset();
  }

}