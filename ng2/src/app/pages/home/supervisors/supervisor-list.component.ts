import { Component, OnInit } from '@angular/core';
import { Supervisor } from 'app/models/supervisor';
import { SupervisorService } from 'app/services/supervisor.service';

@Component({
  selector: 'app-supervisor-list',
  templateUrl: './supervisor-list.component.html',
  styleUrls: ['./supervisor-list.component.css']
})
export class SupervisorListComponent implements OnInit {

  items: Supervisor[] = [];
  selected: Supervisor;

  constructor(private service: SupervisorService) { }

  ngOnInit(): void {
    this.reset();
  }

  public select(supervisor: Supervisor): void {
    this.getById(supervisor.id);
  }

  private list(): void {
    this.service.all().subscribe((response) => {
      if(response){
        this.items = response;
      } else {
        this.items = [];
      }
    });
  }

  public submitted(event): void {
    this.reset();
  }

  private getById(id: number) {
    this.service.get(id).subscribe((response) => {
      this.selected = response;
    });
  }

  private reset() {
    this.selected = this.service.reset();
    this.list();
  }
}