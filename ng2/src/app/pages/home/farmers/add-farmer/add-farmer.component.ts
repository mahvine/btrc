import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Entity } from 'app/models/entity';
import { EntityService } from 'app/services/entity.service';
import { Farmer } from 'app/models/farmer';
import { FarmerService } from 'app/services/farmer.service';


@Component({
  selector: 'app-add-farmer',
  templateUrl: './add-farmer.component.html',
  styleUrls: ['./add-farmer.component.css']
})
export class AddFarmerComponent implements OnInit {

  @Input() farmer: Farmer;
  @Output() farmerSubmitted: EventEmitter<boolean> = new EventEmitter<boolean>();
  entities: Entity[];
  constructor(private service: FarmerService, private entityService: EntityService) { }


  ngOnInit() {
    this.reset();
    this.entityService.all().subscribe(response => {
      this.entities = response;
    });
  }

  public submit(entity: Farmer): void {
    this.service.post(entity).subscribe((response) => {
      this.reset();
      this.farmerSubmitted.emit(true);
    })
  }

  public reset(): void {
    this.farmer = this.service.reset();
  }

}
