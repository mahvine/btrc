import { Component, OnInit } from '@angular/core';
import { Farmer } from 'app/models/farmer';
import { FarmerService } from 'app/services/farmer.service';

@Component({
  selector: 'app-farmer-list',
  templateUrl: './farmer-list.component.html',
  styleUrls: ['./farmer-list.component.css']
})
export class FarmerListComponent implements OnInit {

  items: Farmer[] = [];
  selected: Farmer;

  constructor(private service: FarmerService) { }

  ngOnInit(): void {
    this.reset();
  }

  public select(farmer: Farmer): void {
    this.getById(farmer.id);
  }

  private list(): void {
    this.service.all().subscribe((response) => {
      this.items = response;
    });
  }

  public submitted(event): void {
    this.reset();
  }

  private getById(id: number) {
    this.service.get(id).subscribe((response) => {
      this.selected = response;
    });
  }

  private reset() {
    this.selected = this.service.reset();
    this.list();
  }
}