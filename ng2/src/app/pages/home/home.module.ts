import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../components/components.module';
import { HomeRoutingModule } from './home.routing';

import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { EntityListComponent } from './entities/entity-list.component';
import { ManagerListComponent } from './area-manager/manager-list.component';
import { FarmerListComponent } from './farmers/farmer-list.component';
import { CropListComponent } from './crops/crop-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';

import { HttpClientModule } from '@angular/common/http';
import { AddCropsComponent } from './crops/add-crops/add-crops.component';
import { HttpModule } from '@angular/http';
import { ServicesModule } from 'app/services/services.module';
import { AddEntityComponent } from './entities/add-entity/add-entity.component';
import { SupervisorListComponent } from './supervisors/supervisor-list.component';
import { AddManagerComponent } from './area-manager/add-manager/add-manager.component';
import { AddSupervisorComponent } from './supervisors/add-supervisor/add-supervisor.component';
import { AddFarmerComponent } from './farmers/add-farmer/add-farmer.component';
import { OrderListComponent } from './orders/order-list.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ComponentsModule,
    HomeRoutingModule,
    RouterModule,
    HttpModule,
    ServicesModule
  ],
  declarations: [
    HomeComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    EntityListComponent, AddEntityComponent, 
    ManagerListComponent, AddManagerComponent,
    FarmerListComponent, AddFarmerComponent,
    SupervisorListComponent, AddSupervisorComponent, 
    OrderListComponent, AddOrderComponent,
    CropListComponent, AddCropsComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent
  ],
  providers: [
  ]
})
export class HomeModule { }
