export class Entity {
    id: number;
    name: String;
    description: String;
    address: String;
    contactNumber: String;
  }
  