import {Entity} from './entity';

export class Farmer {
  id: number;
  name: String;
  city: String;
  address: String;
  contactNumber: String;
  farmArea?: number;
  poolEntity: Entity;
}
