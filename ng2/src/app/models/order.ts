import {Crop} from 'app/models/crop';

export class Order {
    id: number;
	
	buyerName: String;
	
	buyerEmail?: String;
	
	buyerContact?: String;
	
	buyerAddress?: String;
	
	dropOff?: String;

	dateReceived?: Date;

	dateFulfilled?: Date;
	
	status: String;
	
	crops: Crop[];
}