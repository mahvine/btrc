export interface Crop {
    id: number;
    name: string;
    sellingPrice: number;
    sharePerSold: number;
    yieldPerSqm: number;
    plantingToHarvestDuration: number;
    plantingToHarvestUnits: string;
    volumeUnits: string;
}
