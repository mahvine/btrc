import {Entity} from './entity';

export class Supervisor {
  id: number;
  name: String;
  city: String;
  address: String;
  contactNumber: String;
  poolEntity: Entity;
}
