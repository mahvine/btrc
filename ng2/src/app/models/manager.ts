import {Entity} from './entity';

export class Manager {
  id: number;
  name: String;
  city: String;
  address: String;
  contactNumber: String;
  poolEntity: Entity;
}
