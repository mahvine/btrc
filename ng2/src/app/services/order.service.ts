import { Injectable } from '@angular/core';
import { Order } from '../models/order';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

@Injectable()
export class OrderService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  all(): Observable<Order[]> {
    return this.http.get<Order[]>(this.apiUrl + '/orders').map((response) => response);
  }

  get(id: number): Observable<Order> {
    return this.http.get<Order>(this.apiUrl + `/orders/${id}`).map((response) => response);
  }

  post(order: Order): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.apiUrl + '/orders', order, { headers: headers }).map((response) => response);
  }

  reset(): Order {
    return {
      id: 0,
      buyerName: '',
      buyerAddress: '',
      buyerContact: '',
      crops: [],
      status: 'NEW'
    };
  }
}