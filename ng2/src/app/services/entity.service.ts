import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { Entity } from 'app/models/entity';

@Injectable()
export class EntityService {
  private apiUrl = environment.apiUrl;
  private URI = '/entities';
  constructor(private http: HttpClient) { }

  all(): Observable<Entity[]> {
    return this.http.get<Entity[]>(this.apiUrl + this.URI).map((response) => response);
  }

  get(id: number): Observable<Entity> {
    return this.http.get<Entity>(this.apiUrl + this.URI+`/${id}`).map((response) => response);
  }

  post(entity: Entity): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.apiUrl + this.URI, entity, { headers: headers }).map((response) => response);
  }

  reset(): Entity {
    return {
      id: 0,
      name: '',
      description: '',
      address: '',
      contactNumber: ''
    };
  }

}
