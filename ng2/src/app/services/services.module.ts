import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CropService } from './crop.service';
import { EntityService } from 'app/services/entity.service';
import { ManagerService } from 'app/services/manager.service';
import { FarmerService } from 'app/services/farmer.service';
import { SupervisorService } from 'app/services/supervisor.service';
import { OrderService } from 'app/services/order.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers:[
    CropService,
    EntityService,
    ManagerService,
    FarmerService,
    SupervisorService,
    OrderService
  ]
})
export class ServicesModule { }
