import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { Supervisor } from 'app/models/supervisor';

@Injectable()
export class SupervisorService {

  
  private apiUrl = environment.apiUrl;
  private URI = '/farmSupervisors';
  constructor(private http: HttpClient) { }

  all(): Observable<Supervisor[]> {
    return this.http.get<Supervisor[]>(this.apiUrl + this.URI).map((response) => response);
  }

  get(id: number): Observable<Supervisor> {
    return this.http.get<Supervisor>(this.apiUrl + this.URI+`/${id}`).map((response) => response);
  }

  post(supervisor: Supervisor): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    if(supervisor.poolEntity === undefined){
      throw new Error('no pool entity found');
    }
    return this.http.post(this.apiUrl + this.URI, supervisor, { headers: headers }).map((response) => response);
  }

  reset(): Supervisor {
    return {
      id: 0,
      name: '',
      city: '',
      address: '',
      contactNumber: '',
      poolEntity: undefined
    };
  }


}
