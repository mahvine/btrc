import { Injectable } from '@angular/core';
import { Crop } from '../models/crop';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

@Injectable()
export class CropService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  all(): Observable<Crop[]> {
    return this.http.get<Crop[]>(this.apiUrl + '/crops').map((response) => response);
  }

  get(id: number): Observable<Crop> {
    return this.http.get<Crop>(this.apiUrl + `/crops/${id}`).map((response) => response);
  }

  post(crop: Crop): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.apiUrl + '/crops', crop, { headers: headers }).map((response) => response);
  }

  reset(): Crop {
    return {
      yieldPerSqm: 0,
      sharePerSold: 0,
      plantingToHarvestDuration: 0,
      name: '',
      sellingPrice: 0,
      id: 0,
      plantingToHarvestUnits: 'Days',
      volumeUnits: 'kg'
    };
  }
}
