import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { Farmer } from 'app/models/farmer';


@Injectable()
export class FarmerService {

  
  private apiUrl = environment.apiUrl;
  private URI = '/farmers';
  constructor(private http: HttpClient) { }

  all(): Observable<Farmer[]> {
    return this.http.get<Farmer[]>(this.apiUrl + this.URI).map((response) => response);
  }

  get(id: number): Observable<Farmer> {
    return this.http.get<Farmer>(this.apiUrl + this.URI+`/${id}`).map((response) => response);
  }

  post(farmer: Farmer): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    if(farmer.poolEntity === undefined){
      throw new Error('no pool entity found');
    }
    return this.http.post(this.apiUrl + this.URI, farmer, { headers: headers }).map((response) => response);
  }

  reset(): Farmer {
    return {
      id: 0,
      name: '',
      city: '',
      address: '',
      contactNumber: '',
      poolEntity: undefined
    };
  }

}
