import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { Manager } from 'app/models/manager';


@Injectable()
export class ManagerService {

  
  private apiUrl = environment.apiUrl;
  private URI = '/areaManagers';
  constructor(private http: HttpClient) { }

  all(): Observable<Manager[]> {
    return this.http.get<Manager[]>(this.apiUrl + this.URI).map((response) => response);
  }

  get(id: number): Observable<Manager> {
    return this.http.get<Manager>(this.apiUrl + this.URI+`/${id}`).map((response) => response);
  }

  post(manager: Manager): any {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    if(manager.poolEntity === undefined){
      throw new Error('no pool entity found');
    }
    return this.http.post(this.apiUrl + this.URI, manager, { headers: headers }).map((response) => response);
  }

  reset(): Manager {
    return {
      id: 0,
      name: '',
      city: '',
      address: '',
      contactNumber: '',
      poolEntity: undefined
    };
  }

}
