package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.model.Order;
import ph.greenminds.repository.OrderRepository;
import ph.greenminds.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api/orders")
public class OrderResource {

	private static final Logger logger = LoggerFactory.getLogger(OrderResource.class);

	@Inject
	OrderRepository orderRepository;

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody Order order, HttpServletRequest request) {
		orderRepository.save(order);
		logger.info("Saved order:{}",order);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Order>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<Order> pageResult = orderRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities", page, size);
		return new ResponseEntity<List<Order>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{orderId}", method = RequestMethod.GET)
	public Order getById(@PathVariable("orderId") Long id) {
		return orderRepository.findOne(id);
	}

}
