package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.dto.UserDTO;
import ph.greenminds.model.User;
import ph.greenminds.repository.UserRepository;
import ph.greenminds.service.UserService;
import ph.greenminds.service.AuthorizationService.BtrcRole;
import ph.greenminds.web.rest.util.PaginationUtil;

/**
 * 
 * @author JRDomingo
 *
 */
@RestController
@RequestMapping("/api/farmSupervisors")
public class SupervisorResource {

	private static final Logger logger = LoggerFactory.getLogger(SupervisorResource.class);

	@Inject
	UserRepository userRepository;
	
	@Inject
	UserService userService;

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody UserDTO supervisor, HttpServletRequest request) {
		Long poolEntityId = supervisor.poolEntity != null ? supervisor.poolEntity.id: null;
		userService.saveFarmSupervisor(poolEntityId, supervisor.id, supervisor.name, supervisor.contactNumber, supervisor.address, supervisor.city);
		logger.info("Saved supervisor:{}",supervisor);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<User> pageResult = userRepository.findByType(BtrcRole.FARM_SUPERVISOR.name(), pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/supervisors", page, size);

		List<UserDTO> users = new ArrayList<UserDTO>();
		pageResult.getContent().stream().forEach(user -> users.add(new UserDTO(user)));
		return new ResponseEntity<List<UserDTO>>(users, headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{supervisorId}", method = RequestMethod.GET)
	public UserDTO getById(@PathVariable("supervisorId") Long id) {
		return new UserDTO(userRepository.findOne(id));
	}

}
