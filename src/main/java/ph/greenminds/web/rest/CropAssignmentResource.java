package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.model.CropAssignment;
import ph.greenminds.repository.CropAssignmentRepository;
import ph.greenminds.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api/cropAssignments")
public class CropAssignmentResource {

	private static final Logger logger = LoggerFactory.getLogger(CropAssignmentResource.class);

	@Inject
	CropAssignmentRepository cropAssignmentRepository;

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody CropAssignment cropAssignment, HttpServletRequest request) {
		cropAssignmentRepository.save(cropAssignment);
		logger.info("Saved cropAssignment:{}", cropAssignment);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CropAssignment>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<CropAssignment> pageResult = cropAssignmentRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities", page, size);
		return new ResponseEntity<List<CropAssignment>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{cropAssignmentId}", method = RequestMethod.GET)
	public CropAssignment getById(@PathVariable("cropAssignmentId") Long id) {
		return cropAssignmentRepository.findOne(id);
	}

}
