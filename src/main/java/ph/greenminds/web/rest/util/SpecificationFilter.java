package ph.greenminds.web.rest.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import ph.greenminds.BtrcApplication;


public abstract class SpecificationFilter<T> implements Specification<T>{
	
	private static final Logger logger = LoggerFactory.getLogger(SpecificationFilter.class);
	
	protected Map<String, String[]> parameters;
	
	protected HttpServletRequest request;

	public SpecificationFilter(Map<String, String[]> parameters){
		this.parameters = parameters;
	}

	public SpecificationFilter(HttpServletRequest request){
		this.request = request;
	}
	
	public Map<String,String[]> getParameters(){
		if(request!=null){
			return request.getParameterMap();
		}else{
			return parameters;
		}
	}

	public String getParam(String key){
		if(request!=null){
			return request.getParameter(key);
		}
		if(parameters!=null){
			Object param = parameters.get(key);
			if(param instanceof String[]){
				String[] value  = (String[]) param;
				if(value!=null){
					if(value.length>0){
						return value[0];
					}
				}
			}else if(param instanceof String){
				return (String) param;
			}
		}
		return null;
	}

	public Integer getIntParam(String key){
		String value = getParam(key);
		if(value!=null){
			return Integer.parseInt(value);
		}
		return null;
	}

	public Long getLongParam(String key){
		String value = getParam(key);
		if(value!=null){
			return Long.parseLong(value);
		}
		return null;
	}

	public List<Long> getIdsParam(String key){
		String values = getParam(key);
		if(values!=null){
			List<Long> ids = new ArrayList<Long>();
			String[] commaIds =values.split(",");
			for(String id:commaIds){
				try{
					ids.add(Long.parseLong(id));
				}catch(RuntimeException e){}
			}
			return ids;
		}
		return null;
	}

	public Double getDoubleParam(String key){
		String value = getParam(key);
		if(value!=null){
			return Double.parseDouble(value);
		}
		return null;
	}
	
	public Date getDateParam(String key, String format){
		String value = getParam(key);
		if(value!=null){
			DateFormat df = new SimpleDateFormat(format);
			try{
				return df.parse(value);
			}catch(RuntimeException e){
			} catch (ParseException e) {
			}
		}
		return null;
	}
	
	public Date getDateParam(String key){
		return getDateParam(key, BtrcApplication.DATE_FORMAT);
	}
	
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteria,CriteriaBuilder criteriaBuilder){
		final List<Predicate> predicates = new ArrayList<Predicate>();
		createPredicates(root, criteria, criteriaBuilder, predicates);
		logger.debug("predicates list size:{}",predicates.size());
		if(predicates.isEmpty()){
			return null;
		}
		return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	}

	/**
	 * 	<b>Sample Code:</b><br>
	 * <code>
 	 *	Predicate greaterThanEqualDate = criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("endDate"), date);<br>
 	 *	predicates.add(greaterThanEqualDate);
	 * </code>
	 */
	public abstract void createPredicates(Root<T> root, CriteriaQuery<?> criteria,CriteriaBuilder criteriaBuilder, List<Predicate> predicates);
	
		
}
