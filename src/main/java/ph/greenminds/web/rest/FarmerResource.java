package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.dto.UserDTO;
import ph.greenminds.model.Farmer;
import ph.greenminds.repository.FarmerRepository;
import ph.greenminds.service.UserService;
import ph.greenminds.web.rest.util.PaginationUtil;

/**
 * 
 * @author JRDomingo
 *
 */
@RestController
@RequestMapping("/api/farmers")
public class FarmerResource {

	private static final Logger logger = LoggerFactory.getLogger(FarmerResource.class);

	@Inject
	FarmerRepository farmerRepository;
	
	@Inject
	UserService userService;

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody UserDTO farmer, HttpServletRequest request) {
		Long poolEntityId = farmer.poolEntity != null ? farmer.poolEntity.id: null;
		userService.saveFarmer(poolEntityId, farmer.id, farmer.name, farmer.contactNumber, farmer.address, farmer.city, farmer.area, farmer.farmArea);
		logger.info("Saved farmer:{}",farmer);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "100") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<Farmer> pageResult = farmerRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/farmers", page, size);

		List<UserDTO> users = new ArrayList<UserDTO>();
		pageResult.getContent().stream().forEach(user -> users.add(new UserDTO(user)));
		return new ResponseEntity<List<UserDTO>>(users, headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{farmerId}", method = RequestMethod.GET)
	public UserDTO getById(@PathVariable("farmerId") Long id) {
		return new UserDTO(farmerRepository.findByUserId(id));
	}

}
