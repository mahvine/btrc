package ph.greenminds.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.greenminds.dto.UserDTO;
import ph.greenminds.model.Farmer;
import ph.greenminds.model.PoolEntity;
import ph.greenminds.model.User;
import ph.greenminds.repository.FarmerRepository;
import ph.greenminds.repository.PoolEntityRepository;
import ph.greenminds.repository.UserRepository;
import ph.greenminds.service.AuthorizationService.BtrcRole;
import ph.greenminds.service.UserService;
import ph.greenminds.web.rest.util.PaginationUtil;

/**
 * 
 * @author mahvine
 * @since Feb 12, 2018
 *
 */
@RestController
@RequestMapping("/api/entities")
public class PoolEntityResource {

	private static final Logger logger = LoggerFactory.getLogger(PoolEntityResource.class);

	@Inject
	PoolEntityRepository poolEntityRepository;

	@Inject
	UserRepository userRepository;

	@Inject
	FarmerRepository farmerRepository;
	
	@Inject
	UserService userService;
	
	

	@RequestMapping(method = RequestMethod.POST)
	public void save(@Valid @RequestBody PoolEntity poolEntity, HttpServletRequest request) {
		poolEntityRepository.save(poolEntity);

		logger.info("Saved poolEntity:{}",poolEntity);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<PoolEntity>> listAll(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction) throws URISyntaxException {
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<PoolEntity> pageResult = poolEntityRepository.findAll(pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities", page, size);
		return new ResponseEntity<List<PoolEntity>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/{entityId}", method = RequestMethod.GET)
	public PoolEntity getById(@PathVariable("entityId") Long id) {
		return poolEntityRepository.findOne(id);
	}
	

	@RequestMapping(path = "/{entityId}/areaManagers",method = RequestMethod.POST)
	public User saveAreaManager(@PathVariable("entityId") Long entityId, HttpServletRequest request, @RequestBody UserDTO user) {
		PoolEntity poolEntity = poolEntityRepository.findOne(entityId);
		User areaManager = userService.saveAreaManager(poolEntity.id, user.id, user.name, user.contactNumber, user.address, user.city);
		logger.info("Saved manager:{}",areaManager);
		return areaManager;
	}
	

	@RequestMapping(path = "/{entityId}/areaManagers", method = RequestMethod.GET)
	public ResponseEntity<List<User>> listAllAreaManagers(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "100") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction,
			@PathVariable("entityId") Long entityId) throws URISyntaxException {
		
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<User> pageResult = userRepository.findByTypeAndPoolEntityId(BtrcRole.AREA_MANAGER.toString(), entityId, pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities/"+entityId+"/areaManagers", page, size);
		return new ResponseEntity<List<User>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	

	@RequestMapping(path = "/{entityId}/farmSupervisors",method = RequestMethod.POST)
	public User saveFarmSupervisor(@PathVariable("entityId") Long entityId, HttpServletRequest request, @RequestBody UserDTO user) {
		PoolEntity poolEntity = poolEntityRepository.findOne(entityId);
		logger.info("Saved poolEntity:{}",poolEntity);
		return userService.saveFarmSupervisor(poolEntity.id, user.id, user.name, user.contactNumber, user.address, user.city);
	}
	

	@RequestMapping(path = "/{entityId}/farmSupervisors", method = RequestMethod.GET)
	public ResponseEntity<List<User>> listAllSupervisors(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "100") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction,
			@PathVariable("entityId") Long entityId) throws URISyntaxException {
		
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<User> pageResult = userRepository.findByTypeAndPoolEntityId(BtrcRole.FARM_SUPERVISOR.toString(), entityId, pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities/"+entityId+"/supervisors", page, size);
		return new ResponseEntity<List<User>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	
	
	@RequestMapping(path = "/{entityId}/farmers",method = RequestMethod.POST)
	public UserDTO saveFarmers(@PathVariable("entityId") Long entityId, HttpServletRequest request, @RequestBody UserDTO user) {
		PoolEntity poolEntity = poolEntityRepository.findOne(entityId);
		
		Farmer farmer = userService.saveFarmer(poolEntity.id, user.id, user.name, user.contactNumber, user.address, user.city, user.area, user.farmArea);
		logger.info("Saved farmer:{}",farmer);
		return new UserDTO(farmer);
	}

	@RequestMapping(path = "/{entityId}/farmers/{farmerId}", method = RequestMethod.GET)
	public Farmer getFarmerById(@PathVariable("entityId") Long entityId, @PathVariable("farmerId") Long farmerId) {
		return farmerRepository.findOne(farmerId);
	}
	
	


	@RequestMapping(path = "/{entityId}/farmers", method = RequestMethod.GET)
	public ResponseEntity<List<User>> listAllFarmers(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "size", required = false, defaultValue = "100") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction,
			@PathVariable("entityId") Long entityId) throws URISyntaxException {
		
		Sort sort = new Sort(direction, sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<User> pageResult = userRepository.findByTypeAndPoolEntityId(BtrcRole.FARMER.toString(), entityId, pageRequest);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/entities/"+entityId+"/farmers", page, size);
		return new ResponseEntity<List<User>>(pageResult.getContent(), headers, HttpStatus.OK);
	}


}
