package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.Farmer;

public interface FarmerRepository extends JpaRepository<Farmer, Long>{
	
	Farmer findByUserId(Long userId);

}
