package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

}
