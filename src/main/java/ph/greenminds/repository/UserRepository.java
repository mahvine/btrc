package ph.greenminds.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ph.greenminds.model.User;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

	User findByFullname(String fullname);

	User findByTypeAndPoolEntityId(String type, Long poolEntityId);

	Page<User> findByTypeAndPoolEntityId(String type, Long poolEntityId, Pageable pageRequest);

	Page<User> findByType(String type, Pageable pageRequest);

}
