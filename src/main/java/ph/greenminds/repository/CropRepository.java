package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.Crop;

public interface CropRepository extends JpaRepository<Crop, Long>{

}
