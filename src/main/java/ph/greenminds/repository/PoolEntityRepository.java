package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.PoolEntity;

public interface PoolEntityRepository extends  JpaRepository<PoolEntity, Long>{
	
}
