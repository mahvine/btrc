package ph.greenminds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.greenminds.model.Role;

public interface RoleRepository  extends JpaRepository<Role, Long>{
	
	Role findByName(String name);

}
