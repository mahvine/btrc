package ph.greenminds.service;

import java.util.Date;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ph.greenminds.model.Farmer;
import ph.greenminds.model.PoolEntity;
import ph.greenminds.model.User;
import ph.greenminds.repository.FarmerRepository;
import ph.greenminds.repository.PoolEntityRepository;
import ph.greenminds.repository.UserRepository;
import ph.greenminds.service.AuthorizationService.BtrcRole;

@Service
public class UserService {
	
	@Inject
	private PoolEntityRepository poolEntityRepository;
	
	@Inject
	private UserRepository userRepository;
	
	@Inject
	private AuthorizationService authService;
	
	@Inject
	private FarmerRepository farmerRepository;
	
	public User saveAreaManager(long poolEntityId, Long userId, String fullname, String contactNumber, String address, String city) {
		PoolEntity poolEntity = poolEntityRepository.findOne(poolEntityId);
		if (poolEntity == null) {
			throw new RuntimeException("No pool entity found: "+poolEntityId);
		}
		
		User user = userRepository.findByFullname(fullname);
		if (user == null) {
			user = new User();
			user.username = new Date().getTime()+"";
			user.fullname= fullname;
			
		}
		if(!user.roles.contains(authService.getAreaManagerRole())){			
			user.roles.add(authService.getAreaManagerRole());
		}
		user.poolEntity = poolEntity;
		user.fullname= fullname;
		user.address = address;
		user.contactNumber = contactNumber;
		user.city = city;
		user.type = BtrcRole.AREA_MANAGER.name();
		userRepository.save(user);
		return user;
		
	}
	
	public User saveFarmSupervisor(long poolEntityId, Long userId, String fullname, String contactNumber, String address, String city) {
		PoolEntity poolEntity = poolEntityRepository.findOne(poolEntityId);
		if (poolEntity == null) {
			throw new RuntimeException("No pool entity found: "+poolEntityId);
		}

		User user = userRepository.findByFullname(fullname);
		if (user == null) {
			user = new User();
			user.username = new Date().getTime()+"";
			user.fullname= fullname;
			
		}
		if(!user.roles.contains(authService.getFarmSupervisorRole())){			
			user.roles.add(authService.getFarmSupervisorRole());
		}
		user.fullname= fullname;
		user.poolEntity = poolEntity;
		user.contactNumber = contactNumber;
		user.address = address;
		user.city = city;
		user.type = BtrcRole.FARM_SUPERVISOR.name();
		userRepository.save(user);
		return user;
	}
	
	public Farmer saveFarmer(long poolEntityId, Long userId, String fullname, String contactNumber, String address, String city, String area, Long farmArea) {
		PoolEntity poolEntity = poolEntityRepository.findOne(poolEntityId);
		if (poolEntity == null) {
			throw new RuntimeException("No pool entity found: "+poolEntityId);
		}

		User user = userRepository.findOne(userId);
		Farmer farmer = new Farmer();
		if ( user == null) {
			user = new User();
			user.username = new Date().getTime()+"";
			userRepository.save(user);
			farmer.user = user;
			farmerRepository.save(farmer);
		} else {
			farmer = farmerRepository.findByUserId(userId);
		}
		if(!user.roles.contains(authService.getFarmerRole())){			
			user.roles.add(authService.getFarmerRole());
		}

		user.fullname= fullname;
		user.poolEntity = poolEntity;
		user.type = BtrcRole.FARMER.name();
		user.contactNumber = contactNumber;
		user.address = address;
		user.city = city;
		userRepository.save(user);
		farmer.area = area;
		farmer.farmArea = farmArea;
		farmerRepository.save(farmer);
		
		return farmer;
	}
	
	
}
