package ph.greenminds.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.greenminds.model.Role;
import ph.greenminds.repository.RoleRepository;

@Service
public class AuthorizationService {
	
	private static Logger logger = LoggerFactory.getLogger(AuthorizationService.class);

	private RoleRepository roleRepository;
	
	private Role areaManagerRole;
	
	private Role farmSupervisorRole;
	
	private Role farmerRole;
	
	@Inject
	public AuthorizationService(RoleRepository roleRepository) {
		
		//TODO this should not be in the code. it should be in a database migration tool like liquibase
		//CREATE ROLES IF NOT EXISTING
		areaManagerRole = roleRepository.findByName(BtrcRole.AREA_MANAGER.name());
		if(areaManagerRole == null) {
			areaManagerRole = new Role();
			areaManagerRole.name = BtrcRole.AREA_MANAGER.name();
			areaManagerRole = roleRepository.save(areaManagerRole);
			logger.info("Created AREA_MANAGER role");
		} else {
			logger.info("Existing AREA_MANAGER role");
		}
		

		farmSupervisorRole = roleRepository.findByName(BtrcRole.FARM_SUPERVISOR.name());
		if(farmSupervisorRole == null) {
			farmSupervisorRole = new Role();
			farmSupervisorRole.name = BtrcRole.FARM_SUPERVISOR.name();
			farmSupervisorRole = roleRepository.save(farmSupervisorRole);
			logger.info("Created FARM_SUPERVISOR role");
		} else {
			logger.info("Existing FARM_SUPERVISOR role");
		}
		

		farmerRole = roleRepository.findByName(BtrcRole.FARMER.name());
		if(farmerRole == null) {
			farmerRole = new Role();
			farmerRole.name = BtrcRole.FARMER.name();
			farmerRole = roleRepository.save(farmerRole);
			logger.info("Created FARMER role");
		} else {
			logger.info("Existing FARMER role");
		}
	}

	public Role getAreaManagerRole() {
		return areaManagerRole;
	}

	public Role getFarmSupervisorRole() {
		return farmSupervisorRole;
	}
	

	public Role getFarmerRole() {
		return farmerRole;
	}
	
	

	public enum BtrcRole{AREA_MANAGER, FARM_SUPERVISOR, FARMER}
	

}
