package ph.greenminds.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @author mahvine
 * @since Feb 4, 2018
 *
 */

@Data
@javax.persistence.Entity
@Table(name="c_permission")
public class Permission {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@Column(name="name",unique=true, length=100)
	public String name;

}
