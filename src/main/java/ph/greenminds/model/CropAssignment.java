package ph.greenminds.model;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@javax.persistence.Entity
@Table(name="c_crop_assignment")
public class CropAssignment {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@ManyToOne
	@JoinColumn(name="crop_id", referencedColumnName="id")
	public Crop crop;
	
	@ManyToOne
	@JoinColumn(name="farmer_id", referencedColumnName="id")
	public Farmer farmer;
	
	public Double assignedArea;
	
	public String assignedAreaUnit = "sqm";
	
	public Double yield;
	
	public String yieldUnits = "kg";
	
	public Date harvestDate;

}
