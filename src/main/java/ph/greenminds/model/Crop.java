package ph.greenminds.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @author mahvine
 * @since Feb 4, 2018
 *
 */
@Data
@javax.persistence.Entity
@Table(name="c_crop")
public class Crop {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String name;
	
	public Double sellingPrice;
	
	public Double sharePerSold;
	
	public Double yieldPerSqm;
	
	public String volumeUnits = "kilo";

	public Double plantingToHarvestDuration;
	
	public String plantingToHarvestUnits = "days";

}
