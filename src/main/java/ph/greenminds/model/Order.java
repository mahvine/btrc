package ph.greenminds.model;

import java.util.Date;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;


@Data
@javax.persistence.Entity
@Table(name="c_order")
public class Order {


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String buyerName;
	
	public String buyerEmail;
	
	public String buyerContact;
	
	public String buyerAddress;
	
	public String dropOff;

	public Date dateReceived;

	public Date dateFulfilled;
	
	@Enumerated(EnumType.STRING)
	public Status status;
	
	@OneToMany
    @JoinTable(name = "c_orders",
        joinColumns = @JoinColumn(name = "order_id"),
        inverseJoinColumns = @JoinColumn(name = "crops_id"))
	public List<Crop> crops;
	
	
	public enum Status {
		NEW,UNASSIGNED,DELIVERED,HARVESTED,PARTIALLY_ASSIGNED, FULL_ASSIGNED
	}
}
