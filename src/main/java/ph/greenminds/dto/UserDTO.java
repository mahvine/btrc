package ph.greenminds.dto;

import ph.greenminds.model.Farmer;
import ph.greenminds.model.PoolEntity;
import ph.greenminds.model.User;

public class UserDTO {
	
	public UserDTO(){}
	
	public UserDTO(User user){
		this.address = user.address;
		this.id = user.id;
		this.name = user.fullname;
		this.contactNumber = user.contactNumber;
		this.city = user.city;
		this.area = user.area;
		this.poolEntity = user.poolEntity;
	}
	
	public UserDTO(Farmer farmer){
		this(farmer.user);
		this.farmArea = farmer.farmArea;
		
	}
	
	

	public Long id;
	
	public String name;
	
	public String contactNumber;
	
	public String address;
	
	public String city;
	
	public String area;
	
	public Long farmArea;
	
	public PoolEntity poolEntity;
	
}
